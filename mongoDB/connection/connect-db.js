const mongoose = require("mongoose");
const db = {};
db.mongoose = mongoose;
db.url = process.env.DB_URL;
db.tabs = require("../models/model-tabs")(mongoose);

module.exports = db;