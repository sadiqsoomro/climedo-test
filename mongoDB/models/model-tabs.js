module.exports = mongoose => {
    const Tabs = mongoose.model(
      "tabs",
      mongoose.Schema(
        {
          name: String,
          description: String,
          dataPoints: Array
        },
        { timestamps: false }
      )
    );
  
    return Tabs;
  };