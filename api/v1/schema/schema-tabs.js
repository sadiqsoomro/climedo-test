const Joi = require('joi')

// current schema allows a json object with three keys name description and datapoints which are all compulsory to exist
//datapoints key contains specific array of objects to be allowed.

module.exports.postDataPoints = Joi.object({
    name : Joi.string().required(),
    description: Joi.string().required(),
    dataPoints: Joi.array().items(Joi.object({
        dataType: Joi.string().valid('selection','text','number','date').required(),
        label:Joi.string().required(),
        description:Joi.string().required(),
        options: Joi.array().items(Joi.string().valid("0","1","2","3","4","5","unknown")).optional(),
        placeholder : Joi.string().optional()
     })).required()
})