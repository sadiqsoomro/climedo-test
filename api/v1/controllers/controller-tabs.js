const db = require("../../../mongoDB/connection/connect-db");
const Tabs = db.tabs;
const schema = require('../schema/schema-tabs');
//virtual schema is built using JOI so that the data is only accepted in specified format



// Create and Save a Tab in mongoDB
exports.createTab = async (req, res) => {
  let requestBody = req.body;
  let validation = await schema.postDataPoints.validate(requestBody);
  if (validation.error) {
    res.status(400).json( {error : validation.error.message})
    return;
  }

  const tabs = new Tabs({
    name: requestBody.name,
    description: requestBody.description,
    dataPoints: requestBody.dataPoints 
  });

  // Save tabs in the database
  tabs
    .save(tabs)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the tab."
      });
    });
};

// Retrieve all tabs from the database.
exports.getAllTabs = (req, res) => {
 

  Tabs.find()
    .then(data => {
      res.send({  data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tab."
      });
    });
};

// Update a tab by the id in the request
exports.updateTab = async (req, res) => {
  let requestBody = req.body;
  let validation = await schema.postDataPoints.validate(requestBody);
  if (validation.error) {
    res.status(400).json( {error : validation.error.message})
    return;
  }

  const id = req.params.tabId;

  Tabs.findByIdAndUpdate(id, requestBody, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update tab with id=${id}. `
        });
      } else res.send({ message: "Tab was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Tab with id=" + id
      });
    });
};

// Delete a tab with the specified id in the request
exports.deleteTab = (req, res) => {
  const id = req.params.tabId;

  Tabs.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Tab with id=${id}.`
        });
      } else {
        res.send({
          message: "Tab was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete tab with id=" + id
      });
    });
};


//optional
exports.getTabStats = (req, res) => {
  Tabs.find({}, { _id: 1, dataPoints:1 } )
    .then(data => {
      let array = [];
      for(let loop = 0;loop<data.length;loop++){
      let response = {}; 
      response._id = data[loop]._id.toString();
      response.datapointCount = data[loop].dataPoints.length
      array.push(response);
      }
      //in hurry, took it from internet, sorry.
      let sortedArray = array.sort((a, b) => parseFloat(b.datapointCount) - parseFloat(a.datapointCount));

      res.send({ data: sortedArray })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tab."
      });
    });
};



//ignore this route
exports.pingRoute = (req, res) => {
  res.send({ data: "success" })
};