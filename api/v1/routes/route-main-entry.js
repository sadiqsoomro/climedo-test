module.exports = function (app) {

    // each request lands in this file to check if that url exists, 
    //if it doesnt routes-invalid-route is called to prompt the error
    require('./route-tabs')(app),
    require('./routes-invalid-route')(app)
    
}