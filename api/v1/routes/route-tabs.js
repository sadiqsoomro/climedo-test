module.exports = function (app) {
    //depending on which url is matched, the controller is called accordingly 
    const controllerTabs = require('../controllers/controller-tabs')
    
    //ignore this route
    app.get('/pingRoute', function (req, res) {
        controllerTabs.pingRoute(req, res)
    })

    app.post('/tabs', function (req, res) {
        controllerTabs.createTab(req, res)
    })
    
    app.put('/tabs/:tabId', function (req, res) {
        controllerTabs.updateTab(req, res)
    })

    app.get('/tabs', function (req, res) {
        controllerTabs.getAllTabs(req, res)
    })
    
    app.delete('/tabs/:tabId', function (req, res) {
        controllerTabs.deleteTab(req, res)
    })
    
    app.get('/tabStats', function (req, res) {
        controllerTabs.getTabStats(req, res)
    })
}
