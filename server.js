//Main entry point which is set in package.json

//this is required to call local .env file which is used to set the configuration 
require('dotenv').config();

//require of modules
const express = require('express');
const app = express();
const cors = require('cors');
const PORT = process.env.PORT || 3000;
const db = require("./mongoDB/connection/connect-db");
const routes = require('./api/v1/routes/route-main-entry');


app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use(cors())
routes(app)

db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}.`);
        });

    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });
